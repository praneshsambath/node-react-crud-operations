import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

class App extends React.Component {
  data = {};
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isUpdate: false
    }
  }
  Î
  componentDidMount = () => {
    this.getData();
  }
  getData = () => {
    axios.get('http://localhost:1000/employees')
      .then(response => {
        // handle success
        this.setState({ data: response.data })
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .finally(function () {
        // always executed
      });
  }
  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.isUpdate) {
      this.data = { EmpID: this.empID.value, EmpCode: this.empCode.value, Name: this.name.value, Salary: this.salary.value };
      axios.put('http://localhost:1000/employees', this.data)
        .then(response => {
          this.setState({ isUpdate: false })
          this.empID.value = '';
          this.empCode.value = '';
          this.name.value = '';
          this.salary.value = '';
          this.getData();
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      this.data = { EmpID: this.empID.value, EmpCode: this.empCode.value, Name: this.name.value, Salary: this.salary.value };
      axios.post('http://localhost:1000/employees', this.data)
        .then(response => {
          this.empID.value = '';
          this.empCode.value = '';
          this.name.value = '';
          this.salary.value = '';
          this.getData();
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }
  deleteEmployee = (data) => {

    axios.delete(`http://localhost:1000/employees/${data.EmpID}`)
      .then(response => {
        console.log(response)
        this.getData();
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  updateEmployee = (data) => {
    this.empID.value = data.EmpID;
    this.empCode.value = data.EmpCode;
    this.name.value = data.Name;
    this.salary.value = data.Salary;
    this.setState({ isUpdate: true })
  }
  render() {
    return (
      <div style={{ marginTop: 20 }}>
        <div style={{ padding: 20 }} >
          <div style={{ float: 'left' }}>
            <form onSubmit={this.handleSubmit} >
              <input type='number' disabled={this.state.isUpdate} ref={value => (this.empID = value)}
                placeholder="employee ID" />

              <input type='text' ref={value => (this.empCode = value)} name="EmpCode" placeholder='EmpCode' />
              <input type='text' ref={value => (this.name = value)} name="Name" placeholder='Name' />
              <input type='number' ref={value => (this.salary = value)} name="Salary" placeholder='Salary' />
              <button type='submit' value="submit" >{this.state.isUpdate ? 'UPDATE' : 'SUBMIT'}</button>
            </form>
          </div>
        </div>
        <br />
        <div>
          <table className="table">
            <thead className="thead-dark">
              <tr >
                <th scope="col">Emp ID</th>
                <th scope="col">Emp Code</th>
                <th scope="col">Name</th>
                <th scope="col">Salary</th>
                <th scope="col">Delete</th>
                <th scope="col">Update</th>
              </tr>
            </thead>
            {/* {this.state.data.length === 0 ? <div style={{ textAlign: 'center' }}>NO DATA</div> : */}
            <tbody>
              {this.state.data.length !== 0 ? this.state.data.map((item, index) => (
                <tr key={index}>
                  <th scope="row">{item.EmpID}</th>
                  <td>{item.EmpCode}</td>
                  <td>{item.Name}</td>
                  <td>{item.Salary}</td>
                  <td onClick={this.updateEmployee.bind(this, item)}><span style={{ color: 'blue', cursor: 'pointer' }}>Edit</span></td>
                  <td onClick={this.deleteEmployee.bind(this, item)}><span style={{ color: 'blue', cursor: 'pointer' }}>delete</span></td>
                </tr>
              )) : <tr><th >no data</th></tr>}
            </tbody>
            {/* } */}
          </table>
        </div>
      </div>
    );
  }
}



export default App;
