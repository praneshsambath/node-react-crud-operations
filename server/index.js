const express = require('express');
const app = express();
const mysql = require('mysql');
const bodyparser = require('body-parser');
var http = require('http');

app.use(bodyparser.json());
// app.use(express.static('public'));
app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET,HEAD,OPTIONS,POST,PUT,DELETE"
    );
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
    );
    res.setHeader("Content-Type", "application/x-www-form-urlencoded");
    //and remove cacheing so we get the most recent comments
    res.setHeader("Cache-Control", "no-cache");
    next();
});


let mysql_connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '9843279919',
    database: 'Emplyee DB',
    multipleStatements: true
})

mysql_connection.connect((err) => {
    if (!err)
        console.log('DB Connection success');
    else
        console.log(err);

})

app.listen('1000', () => { console.log('Running on port 1000') });

//GET ALL EMPLOYEE DETAILS
app.get('/employees', (req, res) => {
    mysql_connection.query('SELECT * from Employee', (err, rows, fields) => {
        console.log(fields)
        res.send(rows)
    })

});
//GET INDIVIDUAL EMPLOYEE DETAILS
app.get('/employees/:id', (req, res) => {
    mysql_connection.query('SELECT * from Employee WHERE EmpID = ?', [req.params.id], (err, rows, fields) => {
        res.send(rows)
    })

});
//DELETE INDIVIDUAL EMPLOYEE DETAILS
app.delete('/employees/:id', (req, res) => {
    mysql_connection.query('DELETE FROM Employee WHERE EmpID = ?', [req.params.id], (err, rows, fields) => {
        res.send(rows)
    })

});
//POST INDIVIDUAL EMPLOYEE DETAILS
app.post('/employees', (req, res) => {
    let emp = req.body;

    mysql_connection.query(`INSERT INTO employee (EmpID, Name, EmpCode, Salary) VALUES (?,?,?,?)`, [emp.EmpID, emp.Name, emp.EmpCode, emp.Salary], (err, rows, fields) => {
        if (!err) {
            // rows.forEach(element => {
            //     if (element.constructor == Array) {
            res.send('Added successfully')
            //     }
            // });
        } else {
            console.log(err)
        }
    })

});
//UPDATE INDIVIDUAL EMPLOYEE DETAILS
app.put('/employees', (req, res) => {
    let emp = req.body;

    console.log(data)
    const query = `UPDATE employee SET  ? WHERE ?` ;
    mysql_connection.query(query,[{ Name: emp.Name, EmpCode: emp.EmpCode, Salary: emp.Salary }, { EmpID: emp.EmpID }],(err, rows, fields) => {
        if (!err) {
            // rows.forEach(element => {
            //     if (element.constructor == Array) {
            res.send('Updated successfully')
            //     }
            // });
        } else {
            console.log(err)
        }
    })

});

